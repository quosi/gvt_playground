function drawLines() {
  var canvas = document.getElementById('canvas2');
  var gl = canvas.getContext('experimental-webgl');
  gl.clearColor(0.2, 0.4, 0.2, 0.5);

  var vectorSource = 'attribute vec2 pos;'+
      'void main(){ gl_Position = vec4(pos*0.25, 0, 4); }';
  var vs = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vs, vectorSource);
  gl.compileShader(vs);

  fsSouce =  'void main() { gl_FragColor = vec4(0.8,0.4,0,1); }';
  var fs = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fs, fsSouce);
  gl.compileShader(fs);

  var prog = gl.createProgram();
  gl.attachShader(prog, vs);
  gl.attachShader(prog, fs);
  gl.linkProgram(prog);
  gl.useProgram(prog);

  var vertices = new Float32Array([
    -1,11,
    -9,12,
    -13,9,
    -10,10,
    -3,9,
    -8,7,
    -12,2,
    -6,6,
    -2,7,
    -1,0,
    -1,-5,
    -5,-5,
    -5,-12,
    5,-12,
    5,-5,
    2,-5,
    1,0,
    1,6,
    5,5,
    9,2,
    7,5,
    3,8,
    6,7,
    11,3,
    7,9,
    2,10,
    4,11,
    9,10,
    13,7,
    9,12,
    3,13,
    -1,11
      ]);
  var vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var posAttrib = gl.getAttribLocation(prog, 'pos');
  gl.vertexAttribPointer(posAttrib, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(posAttrib);

  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.drawArrays(gl.LINE_STRIP, 0, 32);
};
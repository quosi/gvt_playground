function loadImage(filename) {
	var imageObj = new Image();
	imageObj.onload = function() {
		var img = document.getElementById('y');
		img.setAttribute('src', this.src);
	};

	imageObj.src = "textures/" + filename;

	var imgArray = ['squ1.png', 'squ2.png', 'squ3.png', 'squ4.png','squ5.png', 'squ6.png', 'squ7.png'];
	var i = 0;
	var img_name = filename
	var intervalID

	function arrayDown(){
		i -= 1;
		img_name = imgArray[((i%7)+7)%7];
		imageObj.src = "textures/" + img_name;
	};

	function arrayUp(){
		i += 1;
		img_name = imgArray[((i%7)+7)%7];
		imageObj.src = "textures/" + img_name;
	};

	window.onkeydown = function(evt) {
		console.log(evt);
		var key = evt.which ? evt.which : evt.keyCode;
		var c = String.fromCharCode(key);
		switch (c) {
		case ('L'):
			arrayDown();
			break;
		case ('R'):
			arrayUp();
			break;
		case ('A'):
			intervalID = intervalID ? clearInterval(intervalID) : setInterval(arrayUp, 100);
			break;
		};
	};
}
function loadImage(filename) {
	var imageObj = new Image();
	imageObj.onload = function() {
		var img = document.getElementById('x');
		img.setAttribute('src', this.src);
	};

	imageObj.src = "textures/" + filename;

	var imgArray = ['img1.png', 'img2.png', 'img3.png', 'img4.png','img5.png', 'img6.png', 'img7.png', 'img8.png', 'img9.png', 'img10.png', 'img11.png', 'img12.png','img13.png', 'img14.png', 'img15.png', 'img16.png', 'img17.png', 'img18.png', 'img19.png', 'img20.png','img21.png', 'img22.png', 'img23.png', 'img24.png'];
	var i = 0;
	var img_name = filename
	var intervalID

	function arrayDown(){
		i -= 1;
		img_name = imgArray[((i%24)+24)%24];
		imageObj.src = "textures/" + img_name;
	};

	function arrayUp(){
		i += 1;
		img_name = imgArray[((i%24)+24)%24];
		imageObj.src = "textures/" + img_name;
	};

	window.onkeydown = function(evt) {
		console.log(evt);
		var key = evt.which ? evt.which : evt.keyCode;
		var c = String.fromCharCode(key);
		switch (c) {
		case ('L'):
			arrayDown();
			break;
		case ('R'):
			arrayUp();
			break;
		case ('A'):
			intervalID = intervalID ? clearInterval(intervalID) : setInterval(arrayUp, 300);
			break;
		};
	};
}
function drawShapes() {
  var canvas = document.getElementById('canvas4_1');
  var gl = canvas.getContext('experimental-webgl');

  // clear frame-buffer & setup pipeline:
  gl.clearColor(.97, .97, .97, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Backface culling:
  gl.frontFace(gl.CCW);
  gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.BACK);

  // compile vertex shader:
  var vsSource = '' + 
  'attribute vec3 pos;' + 
  'attribute vec4 col;' + 
  'varying vec4 color;' + 
  'void main(){' + 'color = col;' + 
  'gl_Position = vec4(pos, 1);' +
  '}';
  var vs = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vs, vsSource);
  gl.compileShader(vs);

  // compile fragment shader:
  fsSouce = 'precision mediump float;' + 
				'varying vec4 color;' + 
				'void main() {' + 
				'gl_FragColor = color;' + 
				'}';
  var fs = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fs, fsSouce);
  gl.compileShader(fs);

  // link shader together into program:
  var prog = gl.createProgram();
  gl.attachShader(prog, vs);
  gl.attachShader(prog, fs);
  gl.bindAttribLocation(prog, 0, "pos");
  gl.linkProgram(prog);
  gl.useProgram(prog);

  var vertices, colors, indicesLines, indicesTris;
  createDrop();

  var vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var posAttrib = gl.getAttribLocation(prog, 'pos');
  gl.vertexAttribPointer(posAttrib, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(posAttrib);

  var vboCol = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vboCol);
  gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

  var colAttrib = gl.getAttribLocation(prog, 'col');
  gl.vertexAttribPointer(colAttrib, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colAttrib);

  var iboLines = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesLines, gl.STATIC_DRAW);
  iboLines.numberOfElements = indicesLines.length;

  var iboTris = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesTris, gl.STATIC_DRAW);
  iboTris.numberOfElements = indicesTris.length;

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
  gl.drawElements(gl.TRIANGLES, iboTris.numberOfElements, gl.UNSIGNED_SHORT, 0);

  gl.disableVertexAttribArray(colAttrib);
  gl.vertexAttrib4f(colAttrib, 0, 0, 1, 1);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
  gl.drawElements(gl.LINES, iboLines.numberOfElements, gl.UNSIGNED_SHORT, 0);

  function createDrop(){
  var n = 42;
  var m = 32;

        vertices = new Float32Array(3 * (n + 1) * (m + 1));
        colors = new Float32Array(4 * (n + 1) * (m + 1));

        indicesLines = new Uint16Array(2 * 2 * n * m);
        indicesTris = new Uint16Array(3 * 2 * n * m);

        var du = 2 * Math.PI / n;
        var dv = 2 * Math.PI / m;
        var a = 0.4;
        var b = 1;

        var iLines = 0;
        var iTris = 0;

        for (var i = 0, u = 0; i <= n; i++, u += du) {
            for (var j = 0, v = 0; j <= m; j++, v += dv) {

                var iVertex = i * (m + 1) + j;
                var x = a * (b - Math.cos(u)) * Math.sin(u) * Math.cos(v); 
                var z = a * (b - Math.cos(u)) * Math.sin(u) * Math.sin(v); 
                var y = Math.cos(u); 

                // vertices
                vertices[iVertex * 3] = x;
                vertices[iVertex * 3 + 1] = y;
                vertices[iVertex * 3 + 2] = z;

                // colors
                if (iVertex % 2 == 0) {
                    colors[iVertex * 4] = 0;
                    colors[iVertex * 4 + 1] = 1;
                    colors[iVertex * 4 + 2] = 1;
                    colors[iVertex * 4 + 3] = 1;
                    colors[iVertex * 4 + 4] = 1;
                    colors[iVertex * 4 + 5] = 1;
                    colors[iVertex * 4 + 6] = 0;
                    colors[iVertex * 4 + 7] = 1;
                }

                // indices
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //        
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }
            }
        }
    }
  


  // SECOND CANVAS

  var canvas = document.getElementById('canvas4_2');
  var gl = canvas.getContext('experimental-webgl');

    // clear frame-buffer & setup pipeline:
    gl.clearColor(.97, .97, .97, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);
  
    // Backface culling:
    gl.frontFace(gl.CCW);
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.BACK);
  
    // compile vertex shader:
    var vsSource = '' + 
    'attribute vec3 pos;' + 
    'attribute vec4 col;' + 
    'varying vec4 color;' + 
    'void main(){' + 'color = col;' + 
    'gl_Position = vec4(pos, 1);' +
    '}';
    var vs = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vs, vsSource);
    gl.compileShader(vs);
  
    // compile fragment shader:
    fsSouce = 'precision mediump float;' + 
          'varying vec4 color;' + 
          'void main() {' + 
          'gl_FragColor = color;' + 
          '}';
    var fs = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fs, fsSouce);
    gl.compileShader(fs);
  
    // link shader together into program:
    var prog = gl.createProgram();
    gl.attachShader(prog, vs);
    gl.attachShader(prog, fs);
    gl.bindAttribLocation(prog, 0, "pos");
    gl.linkProgram(prog);
    gl.useProgram(prog);
  
    var vertices, colors, indicesLines, indicesTris;
    createSnail();
  
    var vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
  
    var posAttrib = gl.getAttribLocation(prog, 'pos');
    gl.vertexAttribPointer(posAttrib, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(posAttrib);
  
    var vboCol = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCol);
    gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);
  
    var colAttrib = gl.getAttribLocation(prog, 'col');
    gl.vertexAttribPointer(colAttrib, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(colAttrib);
  
    var iboLines = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesLines, gl.STATIC_DRAW);
    iboLines.numberOfElements = indicesLines.length;
  
    var iboTris = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesTris, gl.STATIC_DRAW);
    iboTris.numberOfElements = indicesTris.length;
  
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
    gl.drawElements(gl.TRIANGLES, iboTris.numberOfElements, gl.UNSIGNED_SHORT, 0);
  
    gl.disableVertexAttribArray(colAttrib);
    gl.vertexAttrib4f(colAttrib, 0, 0, 1, 1);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
    gl.drawElements(gl.LINES, iboLines.numberOfElements, gl.UNSIGNED_SHORT, 0);
  
    function createSnail(){
      var n = 32;
      var m = 32;
    
      vertices = new Float32Array(3 * (n + 1) * (m + 1));
      colors = new Float32Array(4 * (n + 1) * (m + 1));

      indicesLines = new Uint16Array(2 * 2 * n * m);
      indicesTris = new Uint16Array(3 * 2 * n * m);

      var du = 2 * Math.PI / n;
      var dv = 2 * Math.PI / m;
      var a = 4;
      var b = 10;

      var iLines = 0;
      var iTris = 0;

      for (var i = 0, u = 0; i <= n; i++, u += du) {
          for (var j = 0, v = 0; j <= m; j++, v += dv) {

              var iVertex = i * (m + 1) + j;
              var h = Math.pow(Math.E, (u/(6*Math.PI)))
              var x = a * (1-h) * Math.cos(u) * Math.cos(0.5*v) * Math.cos(0.5*v); 
              var z = 1 - Math.pow(Math.E, (u/(b*Math.PI))) - Math.sin(v) + h * Math.sin(v); 
              var y = a * (-1+h) * Math.sin(u) * Math.cos(0.5*v) * Math.cos(0.5*v);

              // vertices
              vertices[iVertex * 3] = x;
              vertices[iVertex * 3 + 1] = y;
              vertices[iVertex * 3 + 2] = z;

              // colors
              if (iVertex % 2 == 0) {
                  colors[iVertex * 4] = 0;
                  colors[iVertex * 4 + 1] = 1;
                  colors[iVertex * 4 + 2] = 0;
                  colors[iVertex * 4 + 3] = 1;
                  colors[iVertex * 4 + 4] = 0;
                  colors[iVertex * 4 + 5] = 1;
                  colors[iVertex * 4 + 6] = 0;
                  colors[iVertex * 4 + 7] = 1;
                }

              // indices
              if (j > 0 && i > 0) {
                  indicesLines[iLines++] = iVertex - 1;
                  indicesLines[iLines++] = iVertex;
                }
              if (j > 0 && i > 0) {
                  indicesLines[iLines++] = iVertex - (m + 1);
                  indicesLines[iLines++] = iVertex;
                } 

              if (j > 0 && i > 0) {
                  indicesTris[iTris++] = iVertex;
                  indicesTris[iTris++] = iVertex - 1;
                  indicesTris[iTris++] = iVertex - (m + 1);
                  //        
                  indicesTris[iTris++] = iVertex - 1;
                  indicesTris[iTris++] = iVertex - (m + 1) - 1;
                  indicesTris[iTris++] = iVertex - (m + 1);
                }
            }
        }
    }

// THIRD CANVAS

  var canvas = document.getElementById('canvas4_3');
  var gl = canvas.getContext('experimental-webgl');

  // clear frame-buffer & setup pipeline:
  gl.clearColor(.97, .97, .97, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Backface culling:
  gl.frontFace(gl.CCW);
  gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.BACK);

  // compile vertex shader:
  var vsSource = '' + 
  'attribute vec3 pos;' + 
  'attribute vec4 col;' + 
  'varying vec4 color;' + 
  'void main(){' + 'color = col;' + 
  'gl_Position = vec4(pos, 1);' +
  '}';
var vs = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vs, vsSource);
gl.compileShader(vs);

  // compile fragment shader:
  fsSouce = 'precision mediump float;' + 
				'varying vec4 color;' + 
				'void main() {' + 
				'gl_FragColor = color;' + 
				'}';
  var fs = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fs, fsSouce);
  gl.compileShader(fs);

  // link shader together into program:
  var prog = gl.createProgram();
  gl.attachShader(prog, vs);
  gl.attachShader(prog, fs);
  gl.bindAttribLocation(prog, 0, "pos");
  gl.linkProgram(prog);
  gl.useProgram(prog);

  var vertices, colors, indicesLines, indicesTris;
  createQspace();

  var vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var posAttrib = gl.getAttribLocation(prog, 'pos');
  gl.vertexAttribPointer(posAttrib, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(posAttrib);

  var vboCol = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vboCol);
  gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

  var colAttrib = gl.getAttribLocation(prog, 'col');
  gl.vertexAttribPointer(colAttrib, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colAttrib);

  var iboLines = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesLines, gl.STATIC_DRAW);
  iboLines.numberOfElements = indicesLines.length;

  var iboTris = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indicesTris, gl.STATIC_DRAW);
  iboTris.numberOfElements = indicesTris.length;

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboTris);
  gl.drawElements(gl.TRIANGLES, iboTris.numberOfElements, gl.UNSIGNED_SHORT, 0);

  gl.disableVertexAttribArray(colAttrib);
  gl.vertexAttrib4f(colAttrib, 0, 0, 1, 1);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iboLines);
  gl.drawElements(gl.LINES, iboLines.numberOfElements, gl.UNSIGNED_SHORT, 0);

  function createQspace(){
        var n = 42;
        var m = 23;

        vertices = new Float32Array(3 * (n + 1) * (m + 1));
        colors = new Float32Array(4 * (n + 1) * (m + 1));

        indicesLines = new Uint16Array(2 * 2 * n * m);
        indicesTris = new Uint16Array(3 * 2 * n * m);

        var du = 2 * Math.PI / n;
        var dv = 2 * Math.PI / m;
        var a = 0.7;

        var iLines = 0;
        var iTris = 0;

        for (var i = 0, u = 0; i <= n; i++, u += du) {
            for (var j = 0, v = 0; j <= m; j++, v += dv) {

                var iVertex = i * (m + 1) + j;
                var x = Math.sin(u-2);
                var z = Math.cos(v-2); 
                var y = a*Math.cos(u+v); 

                // vertices
                vertices[iVertex * 3] = x;
                vertices[iVertex * 3 + 1] = y;
                vertices[iVertex * 3 + 2] = z;

                // colors
                if (iVertex % 2 == 0) {
                    colors[iVertex * 4] = 0;
                    colors[iVertex * 4 + 1] = 1;
                    colors[iVertex * 4 + 2] = 1;
                    colors[iVertex * 4 + 3] = 1;
                    colors[iVertex * 4 + 4] = 1;
                    colors[iVertex * 4 + 5] = 1;
                    colors[iVertex * 4 + 6] = 1;
                    colors[iVertex * 4 + 7] = 1;
                }

                // indices
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - 1;
                    indicesLines[iLines++] = iVertex;
                }
                if (j > 0 && i > 0) {
                    indicesLines[iLines++] = iVertex - (m + 1);
                    indicesLines[iLines++] = iVertex;
                }

                if (j > 0 && i > 0) {
                    indicesTris[iTris++] = iVertex;
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                    //        
                    indicesTris[iTris++] = iVertex - 1;
                    indicesTris[iTris++] = iVertex - (m + 1) - 1;
                    indicesTris[iTris++] = iVertex - (m + 1);
                }
            }
        }
    }
  // FOURS CANVAS

  var canvas = document.getElementById('canvas4_4');
  var gl = canvas.getContext('experimental-webgl');

  // clear frame-buffer & setup pipeline:
  gl.clearColor(.97, .97, .97, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);
};
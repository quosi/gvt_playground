# Graphical Vision Technologies



## 1. Animated turning disc
   
Here a 2D turning disc is simulated by loading different single images of the disc using HTML and JavaScript. The Image-Elements are exchanged showing the disc rotated for i. e. 0°, 15°, 30°,... . 

[Check out the project here](index.html)

**Interaktion:** <br>
Using the keys 'l' and 'r' will turn the disc one step to the left or right direction. The key 'a' will turn the disc automatically forward. Pressing 'a' again will stop the automatic turning mode.



## 🎓 Learning JavaScript and WebGL

(notes will be added here soon)

-------

This page is based on a [Gatsby](https://www.gatsbyjs.org/) [GitLab pages](https://about.gitlab.com/product/pages/) example.

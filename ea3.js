function drawLines() {
  var canvas = document.getElementById('canvas3');
  var gl = canvas.getContext('experimental-webgl');
  // setup pipeline:
  gl.clearColor(0.2, 0.4, 0.2, 0.5);
  // Backface culling:
  gl.frontFace(gl.CCW);
  gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.BACK); // choose: gl.FRONT / gl.BACK

  // compile vertex shader:
  var vectorSource = 'attribute vec3 pos;'+
      'attribute vec4 col;'+
      'varying vec4 color;'+
      'void main(){'+
          'color = col;'+
          'gl_Position = vec4(pos*0.25, 4);'+
      '}';

  var vs = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vs, vectorSource);
  gl.compileShader(vs);

  // compile fragment shader:
  fsSouce = 'precision mediump float;'+
      'varying vec4 color;'+
      'void main() {'+
          'gl_FragColor = color;'+
      '}';

  var fs = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fs, fsSouce);
  gl.compileShader(fs);

  // link shader together:
  var prog = gl.createProgram();
  gl.attachShader(prog, vs);
  gl.attachShader(prog, fs);
  gl.linkProgram(prog);
  gl.useProgram(prog);

  var vertices = new Float32Array([
    -1,11,
    -9,12,
    -13,9,
    -10,10,
    -3,9,
    -8,7,
    -12,2,
    -6,6,
    -2,7,
    -1,0,
    -1,-5,
    -5,-5,
    -5,-12,
    5,-12,
    5,-5,
    2,-5,
    1,0,
    1,6,
    5,5,
    9,2,
    7,5,
    3,8,
    6,7,
    11,3,
    7,9,
    2,10,
    4,11,
    9,10,
    13,7,
    9,12,
    3,13,
    -1,11
  ]);

  // colors of each vertex in rgba:
  var colors = new Float32Array([
    0.8,1,0,1, //0
    1,1,0,1, //1 - yellow
    0.2,0.6,0,1,
    0.0,0.8,0,1,
    0.1,0.8,0,1,
    0.1,0.9,0,1, //5
    0.2,0.6,0,1,
    0.2,1,0,1,
    0,1,0,1,
    0.5,0.4,0.3,1,
    0.4,0.4,0.2,1, //10
    0.3,0.3,0.1,1,
    0.2,0.2,0.1,1,
    0.2,0.2,0.1,1,
    0.3,0.3,0.1,1,
    0.4,0.4,0.3,1, //15 - brown
    0.5,0.4,0.3,1,
    0.4,0.6,0.3,1,
    0,0.8,0,1,
    0,1,0,1,
    0,1,0,1, //20 - green
    0.1,0.7,0.1,1,
    0.2,0.7,0,1,
    0,1,0,1,
    0,1,0,1,
    0.1,0.9,0,2, //25
    0.1,0.8,0,1,
    0.2,1,0,1,
    0.1,0.8,0,1,
    1,0.9,0,1,
    1,1,0,1, //30 - yellow
    0.8,1,0,1
  ]);

  // index infos to connect vertices:
  var indices = new Uint16Array([
    17,21,8,
    17,8,9,
    9,10,15,
    10,11,12,
    10,13,15,
    10,12,13,
    13,14,15,
    9,15,16,
    16,17,9,
    17,18,21,
    18,20,21,
    18,19,20,
    21,25,8,
    21,24,25,
    21,22,24,
    22,23,24,
    25,31,8,
    25,26,31,
    26,27,29,
    27,28,29,
    26,29,30,
    26,30,31,
    31,1,3,
    1,2,3,
    1,3,0,
    0,3,4,
    0,4,8,
    4,5,7,
    5,6,7,
    4,7,8
  ]);

  // setup position vertex buffer object:
  var vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  // bind vertex buffer to attribute variable:
  var posAttrib = gl.getAttribLocation(prog, 'pos');
  gl.vertexAttribPointer(posAttrib, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(posAttrib);


  // setup color vertex buffer object:
  var vboCol = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vboCol);
  gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);

  // bind vertex buffer to attribute variable:
  var colAttrib = gl.getAttribLocation(prog, 'col');
  gl.vertexAttribPointer(colAttrib, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colAttrib);

  // setup index buffer object:
  var ibo = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices,
      gl.STATIC_DRAW);
  ibo.numberOfEmements = indices.length;

  // clear framebuffer and render primitives:
  gl.clear(gl.COLOR_BUFFER_BIT);
  
  // gl.drawArrays(gl.TRIANGLE_FAN, 0, 32);
  gl.drawElements(gl.TRIANGLES, ibo.numberOfEmements, gl.UNSIGNED_SHORT, 0);

};